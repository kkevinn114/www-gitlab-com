require 'spec_helper'
require_relative '../../helpers/quality_charts_helpers'

describe QualityChartsHelpers do
  class FakeTemplate
    extend QualityChartsHelpers
  end

  let(:stage) { 'stage' }
  let(:group) { 'group' }
  let(:severities) { %w[s1 s2] }

  describe '.bugs_open_vs_closed_rate_params' do
    subject { FakeTemplate.bugs_open_vs_closed_rate_params(stage: stage, group: group, severities: severities) }

    it 'returns correct params for chart' do
      expect(subject).to eq(
        {
          dashboard: QualityChartsHelpers::BUGS_DASHBOARD_ID,
          chart: QualityChartsHelpers::BUGS_OPEN_VS_CLOSED_RATE_CHART_ID,
          embed: 'v2',
          aggregation: 'monthly',
          daterange: { days: QualityChartsHelpers::DEFAULT_DATE_RANGE },
          filters: [
            { name: 'stage', value: 'stage' },
            { name: 'team_group', value: 'group' },
            { name: 'issue_severity', value: %w[s1 s2] }
          ],
          visible: %w[stage team_group issue_severity]
        })
    end

    context 'without values' do
      subject { FakeTemplate.bugs_open_vs_closed_rate_params }

      it 'returns correct params for chart' do
        expect(subject).to eq(
          {
            dashboard: QualityChartsHelpers::BUGS_DASHBOARD_ID,
            chart: QualityChartsHelpers::BUGS_OPEN_VS_CLOSED_RATE_CHART_ID,
            embed: 'v2',
            aggregation: 'monthly',
            daterange: { days: QualityChartsHelpers::DEFAULT_DATE_RANGE },
            filters: [
              { name: 'issue_severity', value: QualityChartsHelpers::DEFAULT_SEVERITIES }
            ],
            visible: %w[issue_severity]
          })
      end
    end
  end
end
