---
layout: markdown_page
title: "Azure DevOps Questions and Concerns"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->


Concern: Vendor Lock-in
    * Question to Ask: If we are not an Azure Cloud customer, once I subscribe to Azure DevOps will Microsoft attempt to shift me away from my current cloud vendor to Microsoft Azure Cloud?  Will our Azure DevOps usage and support be easier only if we are Azure Cloud customers?

Concern: Azure DevOps Support 
     * Question to Ask: If we are not 100% on Azure, will Microsoft provide the same support for other Clouds or on-premise deployments that they have for Azure?  (No they will not)

Concern: Cloud-native Initiatives 
     * Question to Ask: With our cloud-native initiatives, will Azure DevOps have the significant advantage as GitLab does when working with solutions such as Kubernetes?

Concern: CI/CD Initiative 
     * Question to Ask: Will Azure DevOps be able to support our CI/CD initiatives given that they are missing key capabilities such as Feature Flags, Review Apps and Deployment Scenarios (canar, incremental, etc.).

Concern: Audits and Compliance 
     * Question to Ask: Can Azure DevOps satisfy our audit and compliance requirements by allowing us to easily respond to audits and create compliance reports?

Concern: Security Issues Response Time 
     * Question to Ask: Does Azure DevOps provide security features and functionality that will reduce the impact of security issues by:
        - Limiting the affect security issues have on scheduling and rework
        - Alerting the right people that need to be involved
        - Identifying who is impacted by the issue
        - Find and fix issues quickly

Concern: Indentifying Vulnerabilities 
     * Question to Ask:  Does Azure DevOps provide security capabilities that will support our current process for finding vulnerabilities around Static Application Security Testing, Dependency Scanning, and/or License Management?
