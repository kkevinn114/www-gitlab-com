---
layout: job_family_page
title: "Principal Pricing Manager"
---

## Role
GitLab is seeking an experienced Principal Pricing Manager to build out a brand new pricing function at the company.  This pricing professional will work closely with the CEO, CMO, CRO, and Product leadership to drive decision making around all aspects of the [pricing model](https://about.gitlab.com/handbook/ceo/pricing/).  This is a strategic and critical role that has the opportunity to make a huge positive impact on GitLab’s top line growth and bottom line profitability.

## Responsibilities
- Drive robust pricing related research and analysis to generate and test hypotheses to optimize GitLab’s pricing model, including pricing metrics, price points, and package construction
- Collaborate effectively with the executive team and product organization to drive complex pricing decisions
- Ensure pricing and packaging policy is effectively implemented into the customer experience
- Define, track and measure pricing performance, defining actionable decision insights on pricing performance, and implement continuous pricing optimization plans
- Build models to inform the impact of price changes on customer acquisition, usage and churn
- Work with the [Growth](https://about.gitlab.com/direction/growth/) teams to increase Average Revenue per User [(ARPU)](https://about.gitlab.com/handbook/sales/#revenue-per-licensed-user-also-known-as-arpu)

## Requirements
- 7+ years of pricing experience.  MBA preferred.  Previous experience in Product Management also a plus.
- Fluency in pricing research methodologies like conjoint and Van Westendorp analysis
- Deep analytical skill and experience creating complex models to forecast the impact of pricing changes
- Expertise in quantitative and qualitative customer validation techniques, including surveys & customer interviewing to test customer segmentation, value metrics, pricing models, etc.
- Demonstrated ability to drive complex cross-functional pricing & packaging decisions, as well as subsequent implementation and success tracking
- Previous experience using customer/market segmentation techniques to effectively map pricing packages to user personas, buyer personas, and/or market segments
- Interest in and capability to rapidly build out a Pricing team once the function gets established
- Ability to use GitLab
