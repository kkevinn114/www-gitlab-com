---
layout: handbook-page-toc
title: "Education Services"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Education Services
Education Services are part of the Professional Services Team within GitLab Customer Success. 

#### Mission

The primary mission is to plan and implement effective and scalable educational offerings to accelerate customer time to value and drive expanding product adoption.

#### Immediate Priorities

- Education Services currently offers live training to customers as an add-on offering. It is actively working to enhance the content structure and quality for this offering to deliver a highly-effective learning experience for PS customers.
- Education Services recently conducted a [job task analysis](https://docs.google.com/spreadsheets/d/114yAXzzUi3bKoOcN6zG4tOZ5I_-SmPU9luO8Ylp5XRI/edit?usp=sharing) to define and align learning objectives with the relevant industry job roles and jobs to be done. 

#### Long Term Goals
There is a wealth of online content available today on You Tube, the GitLab Handbook, GitLab Docs, and even through EdTech platforms such as Udemy. These content pieces are created by some of the best experts in the GitLab ecosystem, and they are effective for explaining the main concepts, principles, and procedures for using GitLab.

Education Services recognizes that learning happens beyond the “explaining” stage in the journey to mastery. Checking for understanding, getting timely feedback, practicing new skills, and applying those skills on the job are each critical stages in the learning cycle.  To facilitate this journey, learning experiences must be interesting, positive, and relevant to maintain the learner's attention and motivation towards mastery.

With these learning quality pillars in mind, Educatin Services has formulated a [comprehensive strategy](https://docs.google.com/presentation/d/1rnSauSssTeYGNd9KAcgPGJNw0cWQ9g5nWs0lMZjiV74/edit?usp=sharing) and [curriculum plan](https://docs.google.com/document/d/1p4u9aRK7_pbErQYyHcC2EBrFPeQI9BBJzWZqqOUqU1Y/edit?usp=sharing) to efficiently offer GitLab customers effective and scalable online learning experiences. The forward-looking plan may include considerations for more prescriptive, formalized learning paths leading to GitLab certifications.

#### Related Project and Issues
- [Education Services Project Issues List](https://gitlab.com/gitlab-com/sales-team/professional-services/education-services/issues)
