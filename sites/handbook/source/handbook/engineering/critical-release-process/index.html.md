---
layout: markdown_page
title: "Critical Security Release Process"
---

## Content Moved

Critical security release process information has been moved from the Handbook to the [release documentation for security releases](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/process.md#critical-security-releases). Please update your links and bookmarks accordingly.
